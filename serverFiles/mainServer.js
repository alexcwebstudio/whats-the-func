let express = require("express");
const admin = require("firebase-admin");
const serviceAccount = require("./whatsTheFuncSecret.json");
const nodemailer = require("nodemailer");
path = require('path'),
fs = require('fs');

let app = express();

// firedbase initalization
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount)
});

// global data base references
const db = admin.firestore();
const collectionReference = db.collection("subscribers");

// function for moving users from subscibers to week one
function sortUserWeek(db, i) {
	let query = collectionReference.where("userWeek", "==", i);
	query.get().then(function (querySnapshot) {
		let data = querySnapshot.docs.map(function (documentSnapshot) {
			return documentSnapshot.data().email;
		});
		let dataSetter = {
			email : data
		};
		db.collection("weeks").doc("week" + i).set(dataSetter);
		return data;
	});
}

// endpoint for handeling sort functions
function userWeeklySort(req, res, next) {
    for (let i = 1; i < 6; i++) {
        sortUserWeek(db, i);
	}
    return res.send('Sort Running');
}
app.get('/weeklysort', userWeeklySort)

// funtion for moving users from one week to another
function updateWeeks(weekIndex) {
	let updateWeeksQuery = collectionReference.where("userWeek",  "==", weekIndex)
	updateWeeksQuery.get()
  .then(snapshot => {
    if (snapshot.empty) {
      console.log('No matching documents.');
      return;
    }
    snapshot.forEach(doc => {
		let currentUserWeek = doc.data().userWeek;
		collectionReference.doc(doc.id).update({ userWeek: currentUserWeek + 1 })
		console.log(doc.id)
	});
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });
}

// endpoint for moveing users throughout weeks
function userWeekUpdate(req, res, next) {
	 for (let x = 1; x < 6; x++) {
		updateWeeks(x);
	 }
    return res.send('Week Update Running');
}
app.get('/weekUpdater', userWeekUpdate)

// functions for sending emails
function getUsers(index) {
	let docRef = db.collection("weeks").doc("week"+index);
	docRef
	  .get()
	  .then(function(doc) {
		if (doc.exists) {
		  let mailQueryArray = [];
		  mailQueryArray.push(doc.data().email);
		  main(mailQueryArray, index).catch(console.error);
		} else {
		  console.log("No such document!");
		}
	  })
	  .catch(function(error) {
		console.log("ERROR getting document:", error);
	  });
  }

  // start nodemailer
  async function main(dataIn, weekNumber) {
	let transporter = nodemailer.createTransport({
	  name: "www.whatsthefunc.com",
	  host: "mail.whatsthefunc.com",
	  port: 465,
	  secure: true,
	  auth: {
		user: "founders@whatsthefunc.com",
		pass: 'h-)VM9Qdar9'
	  },
	  tls: {
		rejectUnauthorized: false
	  }
	});

	let mailList = dataIn;
	//let receiversAddress;

	mailList.forEach(function(addressItem) {
	  receiversAddress = addressItem;
	});

	// loading the template files based on weeks
	let source = fs.readFileSync(path.join(__dirname, './weekTemplates/testing'+weekNumber+'.html'));

	//send mail with defined transport object
	let info = await transporter.sendMail({
	  from: '"Whats The Func" <testone@alexcevicelow.space>',
	  to: 'b7950330@urhen.com',
	  subject: "Whats The Func: Week #" + weekNumber,
	  html: source
	});
	console.log("Message sent: %s", info.messageId);
  }

  // endpoint for moveing users throughout weeks
function emailSender(req, res, next) {
	for(let y = 1; y < 6; y++) {
		getUsers(y);
	}
   return res.send('Emails Are sending');
}
 app.get('/emailsender', emailSender)

app.listen(3000, () => {
    console.log("live on port 3000");
});
