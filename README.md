# Over View

Whatsthefunc.com is a heavy node js intensive app. This app sends users emails once a week with different functions so developers can get better at reading unfamiliar code. Users sign up on the website landing page
and then their email is added to a data base using google's firebase firestore. The endpoints that are used on this project make the task of sending the emails simple.

# Implementations

**Angular/front end**

- angular routing
- firebase and angular
- ngx smooth scroll
- using flex box
- making a responsive site without using a framework
- svg and gradients

 **Node js/backend**
 
- creating end points
- using firebase with node
- installing and configuring firebase
- implementing node mailer
- using the file system
- sending emails from node js
- implementing firebase to work with nodemailer
- firebase querying
- attaching functions to endpoints
 
## Data Model 
**User onboarding data flow**
1. The user registers to join the email list from the homepage.
    - The user is added to the db based on angular firestore.
2. The user then gets added to the master "subscribers" collection.
    - This master collection is important for when users would like to unsubscribe from the list.
3. The sort user function pulls users from the subscribers collection and sorts users into the correct weeks.
    - This function is key for putting users into week one. Other function base their data off the initial sort function.
4. Update week function is important for moving users through the weeks.
    - This is a key feature to make sure content of emails can be reused but users are always getting new content as the move on through the weeks.
5. Node mailer is responsible for sending the emails via the smtp server and the domain email.
    - All data that is provided to the user is dynamic based on their current week.
```mermaid
graph LR
A[ Home page ] -- User Registration --> B(( Master Collection ))
B --Registered user --> D{Sorted into inital weeks}
D --Collections of users --> E((Collections based on weeks))  

