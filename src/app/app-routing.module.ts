import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component'
import { SignUpSuccessComponent } from './sign-up-success/sign-up-success.component'
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'success', component: SignUpSuccessComponent},
  {path: 'privacy', component: PrivacyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
