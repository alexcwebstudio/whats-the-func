import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainctaComponent } from './maincta/maincta.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HowitworksComponent } from './howitworks/howitworks.component';
import { BottomctaComponent } from './bottomcta/bottomcta.component';
import { FooterComponent } from './footer/footer.component';
import { SignUpSuccessComponent } from './sign-up-success/sign-up-success.component';
import { SupportComponent } from './support/support.component';
import { PrivacyComponent } from './privacy/privacy.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainctaComponent,
    HomepageComponent,
    HowitworksComponent,
    BottomctaComponent,
    FooterComponent,
    SignUpSuccessComponent,
    SupportComponent,
    PrivacyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    ScrollToModule.forRoot()
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
