import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainctaComponent } from './maincta.component';

describe('MainctaComponent', () => {
  let component: MainctaComponent;
  let fixture: ComponentFixture<MainctaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainctaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainctaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
