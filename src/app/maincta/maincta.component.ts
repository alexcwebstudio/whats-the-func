import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-maincta',
  templateUrl: './maincta.component.html',
  styleUrls: ['./maincta.component.css']
})

export class MainctaComponent {
  constructor(private afs: AngularFirestore, private router: Router) {}

  emailHasErrorMessage = ' ';
  hasUserSignedUp = localStorage.getItem('userHasSignedUp');

  newUserEmail(newUserEmailData: string) {
    if ( newUserEmailData.match(/[@]/) && this.hasUserSignedUp !== 'true') {
      this.afs.collection('subscribers').add({email: newUserEmailData, userWeek: 1});
      this.router.navigate(['success']);
    } else {
      if (this.hasUserSignedUp === 'true') {
        this.emailHasErrorMessage  = 'You have already signed up';
      } else {
        this.emailHasErrorMessage = 'Please Enter A Valid Email Address';
      }
    }
  }
}

